package com.developer.cyborgorange.androiddevtest;

/**
 * Created by GazAlienware on 12/9/2015.
 * Interface for the comic database
 * Any backend database can be used, as long as it implements the given interface
 */
public interface ComicDatabaseInterface {
    public ComicObject[] getAllComics();
    public ComicObjectExtended getComicDetails(ComicObject comic);
    public int getPublisherAggregate(ComicObject comic);
}
