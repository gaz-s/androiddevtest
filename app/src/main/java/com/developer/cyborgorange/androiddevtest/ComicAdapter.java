package com.developer.cyborgorange.androiddevtest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;

/**
 * Created by GazAlienware on 12/9/2015.
 * A custom adapter to populater the listview of comics
 * uses convertView recycling (repopulates a discarded view with new data to make it reusable)
 * and uses the viewholder  pattern (refrences to view widges are retained, so they do not need to be looked up every time the view is recyclled / refreshed)
 */
public class ComicAdapter extends ArrayAdapter<ComicObject> {

    private ComicObject[] mData;

    public ComicAdapter(Context context, ComicObject[] comics) {
        super(context, 0, comics);
        mData = comics;
    }

    //viewholder pattern; stores view lookup references that will be reused in the event the parent view is recycled
    private static class ViewHolder{
        TextView txtTitle;
        TextView txtPublisher;
        ImageView imgFavourite;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        ComicObject comic = getItem(position);
        // view lookup cache, will be stored in tag
        ViewHolder viewHolder;
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            //new view; construct new references, and store in the tag
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listview_comic_item, parent, false);
            viewHolder.txtTitle = (TextView) convertView.findViewById(R.id.listview_item_txt_title);
            viewHolder.txtPublisher = (TextView) convertView.findViewById(R.id.listview_item_txt_publisher);
            viewHolder.imgFavourite = (ImageView) convertView.findViewById(R.id.listview_item_img_favourite);
            convertView.setTag(viewHolder);
        }else{
            //recycled view; use existing references
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data into the view using the data object
        viewHolder.txtTitle.setText(comic.getTitle());
        viewHolder.txtPublisher.setText(comic.getPublisher());
        int visibility = comic.isFavourite() ? View.VISIBLE : View.INVISIBLE;
        viewHolder.imgFavourite.setVisibility(visibility);
        // Return the completed view to render on screen
        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {
        //sort the base dataset whenever we are notified of a data change (e.g comic being added / removed from favourites)
        //this ensures that favourites stay at the top of the list, and removed favourites return to their origional position,
        //and all entries remain in alphabetical order
        Arrays.sort(mData);
        super.notifyDataSetChanged();
    }
}
