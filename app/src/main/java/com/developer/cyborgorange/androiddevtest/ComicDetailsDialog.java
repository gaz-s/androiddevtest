package com.developer.cyborgorange.androiddevtest;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by GazAlienware on 12/9/2015.
 * Dialog for displaying full comic details
 * Parent must implement ComicDetailsDialogListener (otherwise ClassCastException thrown in onAttach)
 */
public class ComicDetailsDialog extends DialogFragment{
    private ComicObject mComic;
    private int mIndex = -1;
    private View mLayout;
    private ComicDetailsDialogListener mListener;

    //the comic object that we want to display in full detail
    public void setComic(ComicObject comic) {
        mComic = comic;
    }

    //the arrayadapter item index
    public void setIndex(int index) {
        mIndex = index;
    }

    public interface ComicDetailsDialogListener{
        public void onFavouriteStateChanged(ComicObject comic, int index);
    }

    // Override the Fragment.onAttach() method to instantiate the ComicDetailsDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the ComicDetailsDialogListener so we can send events to the host
            mListener = (ComicDetailsDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement ComicDetailsDialogListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //prevents dialog dismissal on rotation
        setRetainInstance(true);
    }

    @Override
    public void onDestroyView() {
        //prevents dialog dismissal on rotation
        if (getDialog() != null && getRetainInstance())
            getDialog().setDismissMessage(null);
        super.onDestroyView();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //construct and return a dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Comic details")
                .setPositiveButton("OK", null);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        mLayout = inflater.inflate(R.layout.dialog_comic, null);
        builder.setView(mLayout);
        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
        ComicDatabaseInterface database = ComicDatabase.getInstance(getActivity());
        populateDialog(database.getComicDetails(mComic), database.getPublisherAggregate(mComic));
        initialiseFavouriteButton();
    }

    private void initialiseFavouriteButton(){
        final String addFavourite = "Add to favourites";
        final String removeFavourite = "Remove from favourites";
        String buttonText = addFavourite;
        if(mComic.isFavourite()){
            buttonText = removeFavourite;
        }
        Button favourite = (Button) mLayout.findViewById(R.id.dialog_comic_btn_favourite);
        favourite.setText(buttonText);
        favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFavouriteStateChanged(mComic, mIndex);
                dismiss();
            }
        });
    }

    private void populateDialog(ComicObjectExtended comic, int publisherAggregate){
        TextView title = (TextView) mLayout.findViewById(R.id.dialog_comic_txt_title);
        TextView publisher = (TextView) mLayout.findViewById(R.id.dialog_comic_txt_publisher);
        TextView country = (TextView) mLayout.findViewById(R.id.dialog_comic_txt_country);
        TextView date = (TextView) mLayout.findViewById(R.id.dialog_comic_txt_date);
        TextView dewey = (TextView) mLayout.findViewById(R.id.dialog_comic_txt_dewey);
        TextView topic = (TextView) mLayout.findViewById(R.id.dialog_comic_txt_topic);
        TextView aggregate = (TextView) mLayout.findViewById(R.id.dialog_comic_txt_aggregate);

        title.setText(comic.getTitle());
        publisher.setText(comic.getPublisher());
        country.setText(comic.getCountryOfPublication());
        date.setText(comic.getDateOfPublication());
        dewey.setText(comic.getDeweyClassification());
        topic.setText(comic.getTopics());
        aggregate.setText(Integer.toString(publisherAggregate));
    }

}
