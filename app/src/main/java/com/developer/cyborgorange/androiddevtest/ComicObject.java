package com.developer.cyborgorange.androiddevtest;

/**
 * Created by GazAlienware on 12/9/2015.
 * Represents a lightweight comic, containing the core information
 * implements Comparable; allowing sorting of multiple comics via Arrays.sort()
 */

public class ComicObject implements Comparable{

    private int mId;
    private String mTitle;
    private String mPublisher;
    private boolean mFavourites;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getPublisher() {
        return mPublisher;
    }

    public void setPublisher(String publisher) {
        this.mPublisher = publisher;
    }

    public boolean isFavourite() {
        return mFavourites;
    }

    public void setFavourite(boolean favourite) {
        this.mFavourites = favourite;
    }

    //facilitates combination favourites and alphabetical comparison
    //favours favourites over alphabetical (favourites will be at top of list)
    //both favourites and non-favourites will be sorted alphabetically
    @Override
    public int compareTo(Object object) {
        ComicObject comic = (ComicObject) object;
        if(isFavourite() && comic.isFavourite()){
            //both objects are favourite; relegate to alphabetic comparison
            return getTitle().compareToIgnoreCase(((ComicObject)comic).getTitle());
        }else if(isFavourite()){
            //this is a favourite, and the object we are comparing against isn't; return -1 (appear BEFORE this non-favourite)
            return -1;
        }else if(((ComicObject) comic).isFavourite()){
            //this is a non-favourite, and the object we are comparing against is; return 1 (appear AFTER this favourite)
            return 1;
        }else{
            //neither comics are marked as favourite, order alphabetically
            return getTitle().compareToIgnoreCase(((ComicObject)comic).getTitle());
        }
    }
}
