package com.developer.cyborgorange.androiddevtest;

/**
 * Created by GazAlienware on 12/9/2015.
 * Represents a detailed comic object; contains all fields for a comic record, extends the core comic object
 */
public class ComicObjectExtended extends ComicObject {

    private String mOtherTitles;
    private String mBlRecordId;
    private String mBnbNumber;
    private String mIsbn;
    private String mName;
    private String mDatesAssociatedWithName;
    private String mTypeOfName;
    private String mRole;
    private String mAllNames;
    private String mSeriesTitle;
    private String mNumberWithinSeries;
    private String mCountryOfPublication;
    private String mPlaceOfPublication;
    private String mDateOfPublication;
    private String mPhysicalDescription;
    private String mDeweyClassification;
    private String mBlShelfmark;
    private String mTopics;
    private String mAbstract;
    private String mNotes;

    public String getNotes() {
        return mNotes;
    }

    public void setNotes(String notes) {
        mNotes = notes;
    }

    public String getOtherTitles() {
        return mOtherTitles;
    }

    public void setOtherTitles(String otherTitles) {
        mOtherTitles = otherTitles;
    }

    public String getBlRecordId() {
        return mBlRecordId;
    }

    public void setBlRecordId(String blRecordId) {
        mBlRecordId = blRecordId;
    }

    public String getBnbNumber() {
        return mBnbNumber;
    }

    public void setBnbNumber(String bnbNumber) {
        mBnbNumber = bnbNumber;
    }

    public String getIsbn() {
        return mIsbn;
    }

    public void setIsbn(String isbn) {
        mIsbn = isbn;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getDatesAssociatedWithName() {
        return mDatesAssociatedWithName;
    }

    public void setDatesAssociatedWithName(String datesAssociatedWithName) {
        mDatesAssociatedWithName = datesAssociatedWithName;
    }

    public String getTypeOfName() {
        return mTypeOfName;
    }

    public void setTypeOfName(String typeOfName) {
        mTypeOfName = typeOfName;
    }

    public String getRole() {
        return mRole;
    }

    public void setRole(String role) {
        mRole = role;
    }

    public String getAllNames() {
        return mAllNames;
    }

    public void setAllNames(String allNames) {
        mAllNames = allNames;
    }

    public String getSeriesTitle() {
        return mSeriesTitle;
    }

    public void setSeriesTitle(String seriesTitle) {
        mSeriesTitle = seriesTitle;
    }

    public String getNumberWithinSeries() {
        return mNumberWithinSeries;
    }

    public void setNumberWithinSeries(String numberWithinSeries) {
        mNumberWithinSeries = numberWithinSeries;
    }

    public String getCountryOfPublication() {
        return mCountryOfPublication;
    }

    public void setCountryOfPublication(String countryOfPublication) {
        mCountryOfPublication = countryOfPublication;
    }

    public String getPlaceOfPublication() {
        return mPlaceOfPublication;
    }

    public void setPlaceOfPublication(String placeOfPublication) {
        mPlaceOfPublication = placeOfPublication;
    }

    public String getDateOfPublication() {
        return mDateOfPublication;
    }

    public void setDateOfPublication(String dateOfPublication) {
        mDateOfPublication = dateOfPublication;
    }

    public String getPhysicalDescription() {
        return mPhysicalDescription;
    }

    public void setPhysicalDescription(String physicalDescription) {
        mPhysicalDescription = physicalDescription;
    }

    public String getDeweyClassification() {
        return mDeweyClassification;
    }

    public void setDeweyClassification(String deweyClassification) {
        mDeweyClassification = deweyClassification;
    }

    public String getBlShelfmark() {
        return mBlShelfmark;
    }

    public void setBlShelfmark(String blShelfmark) {
        mBlShelfmark = blShelfmark;
    }

    public String getTopics() {
        return mTopics;
    }

    public void setTopics(String topics) {
        mTopics = topics;
    }

    public String getAbstract() {
        return mAbstract;
    }

    public void setAbstract(String anAbstract) {
        mAbstract = anAbstract;
    }
}
