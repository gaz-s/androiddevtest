package com.developer.cyborgorange.androiddevtest;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by GazAlienware on 12/9/2015.
 * Controls data retrieval from the comics SQL database
 */
public class ComicDatabase extends SQLiteAssetHelper implements ComicDatabaseInterface {
    //database constants
    private static final String DATABASE_NAME = "ComicsDatabase.db";
    private static final String TABLE_NAME = "ComicsTable";
    private static final int DATABASE_VERSION = 1;

    //constants for ComicsTable field names
    private static final String FIELD_ID = "`_id`";
    private static final String FIELD_TITLE = "`Title`";
    private static final String FIELD_OTHER_TITLES = "`Other titles`";
    private static final String FIELD_BL_RECORD_ID = "`BL record ID`";
    private static final String FIELD_BNB_NUMBER = "`BNB number`";
    private static final String FIELD_ISBN = "`ISBN`";
    private static final String FIELD_NAME = "`Name`";
    private static final String FIELD_DATES_ASSOCIATED_WITH_NAME = "`Dates associated with name`";
    private static final String FIELD_TYPE_OF_NAME = "`Type of name`";
    private static final String FIELD_ROLE = "`Role`";
    private static final String FIELD_ALL_NAMES = "`All names`";
    private static final String FIELD_SERIES_TITLE = "`Series title`";
    private static final String FIELD_NUMBER_WITHIN_SERIES = "`Number within series`";
    private static final String FIELD_COUNTRY_OF_PUBLICATION = "`Country of publication`";
    private static final String FIELD_PLACE_OF_PUBLICATION = "`Place of publication`";
    private static final String FIELD_PUBLISHER = "`Publisher`";
    private static final String FIELD_DATE_OF_PUBLICATION = "`Date of publication`";
    private static final String FIELD_PHYSICAL_DESCRIPTION = "`Physical description`";
    private static final String FIELD_DEWEY_CLASSIFICATION = "`Dewey classification`";
    private static final String FIELD_BL_SHELFMARK = "`BL shelfmark`";
    private static final String FIELD_TOPICS = "`Topics`";
    private static final String FIELD_ABSTRACT = "`Abstract`";
    private static final String FIELD_NOTES = "`Notes`";

    //helper field array for comic item
    private static final String[] FIELDS_COMIC_ITEM = new String[]{FIELD_ID, FIELD_TITLE, FIELD_PUBLISHER};
    
    //helper field array for extended comic item (all fields)
    private static final String[] FIELDS_COMIC_ITEM_EXTENDED = new String[]{
    FIELD_ID,
    FIELD_TITLE, 
    FIELD_OTHER_TITLES, 
    FIELD_BL_RECORD_ID, 
    FIELD_BNB_NUMBER, 
    FIELD_ISBN, 
    FIELD_NAME, 
    FIELD_DATES_ASSOCIATED_WITH_NAME, 
    FIELD_TYPE_OF_NAME, 
    FIELD_ROLE, 
    FIELD_ALL_NAMES, 
    FIELD_SERIES_TITLE, 
    FIELD_NUMBER_WITHIN_SERIES, 
    FIELD_COUNTRY_OF_PUBLICATION, 
    FIELD_PLACE_OF_PUBLICATION, 
    FIELD_PUBLISHER, 
    FIELD_DATE_OF_PUBLICATION, 
    FIELD_PHYSICAL_DESCRIPTION, 
    FIELD_DEWEY_CLASSIFICATION, 
    FIELD_BL_SHELFMARK, 
    FIELD_TOPICS, 
    FIELD_ABSTRACT, 
    FIELD_NOTES};

    //singleton; only one database instance is necessary.
    private static ComicDatabase sInstance;

    public static ComicDatabase getInstance(Context c){
        if(sInstance == null){
            sInstance = new ComicDatabase(c.getApplicationContext());
        }
        return sInstance;
    }

    private ComicDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //custom getColumnIndex method; used to strip quotes from our field constants
    private int getColumnIndex(Cursor c, String field){
        return c.getColumnIndex(field.replace("`", ""));
    }
    
    @Override
    public ComicObject[] getAllComics() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query(false, TABLE_NAME, FIELDS_COMIC_ITEM, null, null, null, null, FIELD_TITLE + " ASC", null);
        if(c.getCount() == 0){
            return null;
        }
        c.moveToFirst();
        ComicObject[] comics = new ComicObject[c.getCount()];
        while(!c.isAfterLast()){
            int position = c.getPosition();
            comics[position] = new ComicObject();
            comics[position].setId(c.getInt(getColumnIndex(c, FIELD_ID)));
            comics[position].setTitle(c.getString(getColumnIndex(c, FIELD_TITLE)));
            comics[position].setPublisher(c.getString(getColumnIndex(c, FIELD_PUBLISHER)));
            c.moveToNext();
        }
        return comics;
    }

    @Override
    public ComicObjectExtended getComicDetails(ComicObject comic) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query(false, TABLE_NAME, FIELDS_COMIC_ITEM_EXTENDED, FIELD_ID + "=?", new String[]{Integer.toString(comic.getId())}, null, null, null, null);
        if(c.getCount() == 0){
            return null;
        }
        ComicObjectExtended comicExtended = new ComicObjectExtended();
        c.moveToFirst();
        while(!c.isAfterLast()){
            int position = c.getPosition();
            comicExtended.setId(c.getInt(getColumnIndex(c, FIELD_ID)));
            comicExtended.setTitle(c.getString(getColumnIndex(c, FIELD_TITLE)));
            comicExtended.setOtherTitles(c.getString(getColumnIndex(c, FIELD_OTHER_TITLES)));
            comicExtended.setBlRecordId(c.getString(getColumnIndex(c, FIELD_BL_RECORD_ID)));
            comicExtended.setBnbNumber(c.getString(getColumnIndex(c, FIELD_BNB_NUMBER)));
            comicExtended.setIsbn(c.getString(getColumnIndex(c, FIELD_ISBN)));
            comicExtended.setName(c.getString(getColumnIndex(c, FIELD_NAME)));
            comicExtended.setDatesAssociatedWithName(c.getString(getColumnIndex(c, FIELD_DATES_ASSOCIATED_WITH_NAME)));
            comicExtended.setTypeOfName(c.getString(getColumnIndex(c, FIELD_TYPE_OF_NAME)));
            comicExtended.setRole(c.getString(getColumnIndex(c, FIELD_ROLE)));
            comicExtended.setAllNames(c.getString(getColumnIndex(c, FIELD_ALL_NAMES)));
            comicExtended.setSeriesTitle(c.getString(getColumnIndex(c, FIELD_SERIES_TITLE)));
            comicExtended.setNumberWithinSeries(c.getString(getColumnIndex(c, FIELD_NUMBER_WITHIN_SERIES)));
            comicExtended.setCountryOfPublication(c.getString(getColumnIndex(c, FIELD_COUNTRY_OF_PUBLICATION)));
            comicExtended.setPlaceOfPublication(c.getString(getColumnIndex(c, FIELD_PLACE_OF_PUBLICATION)));
            comicExtended.setPublisher(c.getString(getColumnIndex(c, FIELD_PUBLISHER)));
            comicExtended.setDateOfPublication(c.getString(getColumnIndex(c, FIELD_DATE_OF_PUBLICATION)));
            comicExtended.setPhysicalDescription(c.getString(getColumnIndex(c, FIELD_PHYSICAL_DESCRIPTION)));
            comicExtended.setDeweyClassification(c.getString(getColumnIndex(c, FIELD_DEWEY_CLASSIFICATION)));
            comicExtended.setBlShelfmark(c.getString(getColumnIndex(c, FIELD_BL_SHELFMARK)));
            comicExtended.setTopics(c.getString(getColumnIndex(c, FIELD_TOPICS)));
            comicExtended.setAbstract(c.getString(getColumnIndex(c, FIELD_ABSTRACT)));
            comicExtended.setNotes(c.getString(getColumnIndex(c, FIELD_NOTES)));
            c.moveToNext();
        }
        return comicExtended;
    }

    @Override
    public int getPublisherAggregate(ComicObject comic) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query(false, TABLE_NAME, new String[]{FIELD_PUBLISHER}, FIELD_PUBLISHER + "=?", new String[]{comic.getPublisher()}, null, null, null, null);
        return c.getCount();
    }
}
