package com.developer.cyborgorange.androiddevtest;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import csvtosql.CsvToSql;

/**
 * Created by GazAlienware on 12/9/2015.
 * Main comic activity - displays a selectable list of comics, user can select a comic to read furtther details,
 * favourite comics to pin them to the top of the list, and view publisher aggregate information
 */
public class ComicActivity extends AppCompatActivity implements ComicDetailsDialog.ComicDetailsDialogListener {
    private ComicDatabaseInterface database;
    private List<Integer> mFavourites = new ArrayList<Integer>();
    private static final int MAX_FAVOURITES = 10;
    private static final String PREFERENCES_FILE = "comicactivity_preferences";
    private static final String PREFERENCE_FAVOURITES = "preference_favourites";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comic);
        database = ComicDatabase.getInstance(this);
        //convertCsvToSql();
    }

    @Override
    protected void onResume() {
        super.onResume();
        restoreFavourites();
        ListView comicList = (ListView) findViewById(R.id.listview_comics);
        ComicObject[] comics = database.getAllComics();
        if(mFavourites.size() > 0) {
            markFavourites(comics);
        }
        ComicAdapter adapter = new ComicAdapter(this, comics);
        comicList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        comicList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ComicObject comic = (ComicObject) parent.getAdapter().getItem(position);
                //instantiate details dialog, pass lightweight comic object (will be used to lookup full record)
                ComicDetailsDialog dialog = new ComicDetailsDialog();
                dialog.setComic(comic);
                dialog.setIndex(position);
                dialog.show(getFragmentManager(), ComicDetailsDialog.class.toString());
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveFavourites();
    }

    private void saveFavourites(){
        //serialize favourites collection into json
        SharedPreferences.Editor editor = getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE).edit();
        String serializedFavourites = new Gson().toJson(mFavourites);
        editor.putString(PREFERENCE_FAVOURITES, serializedFavourites);
        editor.commit();
    }

    private void restoreFavourites(){
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        String serializedFavourites = preferences.getString(PREFERENCE_FAVOURITES, null);
        if(serializedFavourites != null){
            //deserialize json back into collection
            Type collectionType = new TypeToken<ArrayList<Integer>>(){}.getType();
            mFavourites = new Gson().fromJson(serializedFavourites, collectionType);
        }
    }

    //iterate data array and set favourites flags
    private void markFavourites(ComicObject[] comics){
        for(ComicObject comic : comics) {
            for (Integer favourite : mFavourites) {
                if(comic.getId() == favourite){
                    comic.setFavourite(true);
                    break;
                }
            }
        }
    }

    //callback for when ComicDetailsDialog modifies a comics favourites flag
    @Override
    public void onFavouriteStateChanged(ComicObject comic, int index) {
        if(mFavourites.size() == MAX_FAVOURITES && !comic.isFavourite()){
            Toast.makeText(getApplicationContext(), "Unable to add to favourites, the limit of " + Integer.toString(MAX_FAVOURITES) + " has been reached!", Toast.LENGTH_LONG).show();
        }else {
            //we are going to change the current favourite state of the comic
            if(comic.isFavourite()) {
                //so remove it, if it is currently favourited
                mFavourites.remove((Integer)comic.getId());
                Toast.makeText(getApplicationContext(), "Comic removed from favourites", Toast.LENGTH_SHORT).show();
            }else{
                //or add it, if it is not currently favourited
                mFavourites.add(comic.getId());
                Toast.makeText(getApplicationContext(), "Comic added to favourites", Toast.LENGTH_SHORT).show();
            }
            //change the underlying data, and invalidate the views (views will be redrawn with the favourite marker visible / invisible accordingly)
            ListView comicList = (ListView) findViewById(R.id.listview_comics);
            ((ComicObject) comicList.getItemAtPosition(index)).setFavourite(!comic.isFavourite());
            ((ComicAdapter) comicList.getAdapter()).notifyDataSetChanged();
            comicList.invalidateViews();
        }
    }

    /*Method for converting a CSV file to an SQL database*/
    /*not needed in the wild; this was used during development to execute the CSV conversion on an emulator
    /*the resulting SQL database was extracted, is included as an asset, and is utilised as the data source via the  SQLiteAssetHelper lib
     */
//    private void convertCsvToSql(){
//            String CSV_FILENAME = "titles.csv";
//            String TABLE_NAME = "ComicsTable";
//            String DATABASE_NAME = "ComicsDatabase";
//        CsvToSql.getInstance().parseCsv(this, CSV_FILENAME, DATABASE_NAME, TABLE_NAME);
//    }
}
