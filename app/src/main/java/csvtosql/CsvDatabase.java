package csvtosql;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.List;

/**
 * Created by GazAlienware on 12/8/2015.
 * Database controller for taking the given CSV data, and transposing it into a new SQL database and table
 */
public class CsvDatabase extends SQLiteOpenHelper implements CsvDatabaseInterface {

    private static final int DATABASE_VERSION = 1;
    private static String mTableName;
    private static final String FIELD_ID = "_id";

    public CsvDatabase(String databaseName, String tableName, Context context) {
        super(context, databaseName, null, DATABASE_VERSION);
        mTableName = tableName;
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {}

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    @Override
    public boolean convertCsvToSql(List<String[]> records) {
        //separate the fields from the data
        String[] fields = records.get(0);
        records.remove(0);
        //acquire an instance of the DB
        SQLiteDatabase db = getWritableDatabase();
        //create an empty comics table
        createComicsTable(db, fields);
        //we now have an empty table with all the fields, we can insert the record data
        return insertComicsData(db, fields, records);
    }

    /* creates an SQL table from a given list of fields
    Will insert a primary key field called '_id'
     */
    private void createComicsTable(SQLiteDatabase db, String[] fields){
        //if a comics table already exists, remove it
        db.execSQL("DROP TABLE IF EXISTS `" + mTableName + "`");
        //create a new table with a set field name, and an ID field
        String createTable = "CREATE TABLE `" + mTableName + "` (" + FIELD_ID + " INTEGER PRIMARY KEY, ";
        //iterate through the fields, and dynamically add them to the creation statement, as type TEXT
        for(String field : fields){
            createTable += "`" + field + "` TEXT, ";
        }
        //remove the trailing ", "
        createTable = createTable.substring(0, createTable.length() - 2);
        //close the statement, and execute it
        createTable += ")";
        db.execSQL(createTable);
    }

    /* Converts a list of records into SQL records*/
    private boolean insertComicsData(SQLiteDatabase db, String[] fields, List<String[]> records){
        //integrity check; the list of fields should equal the number of elements in a given record
        if(fields.length != records.get(0).length){
            return false;
        }
        //start a transaction; we want to perform all our insert statements at once, then rollback if there are any issues
        //safer and more efficient than individually inserting each row
        boolean result = false;
        db.beginTransaction();
        try {
            //iterate each record
            for (String[] record : records) {
                ContentValues cv = new ContentValues();
                //for each record, iterate each field, and set its key-value pair
                for(int i = 0; i < fields.length; i++){
                    //cv.put("`" + fields[i] + "`", "`" + record[i] + "`");
                    cv.put("`" + fields[i] + "`", record[i]);
                }
                //insert the record
                db.insert(mTableName, null, cv);
            }
            //once all records have been processed, mark the transaction as successful
            db.setTransactionSuccessful();
            result = true;
        }finally {
            //if transaction was not marked as successful at this point, it will be rolled back, otherwise it will be committed
            db.endTransaction();
        }
        return result;
    }
}




