package csvtosql;

import android.content.Context;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Created by GazAlienware on 12/8/2015.
 * Converts a given CSV file, stored in the assets folder, to an SQL database
 * Flexible enough to handle any CSV file
 */
public class CsvToSql {

    private static CsvToSql sInstance = new CsvToSql();
    public static CsvToSql getInstance() {
        return sInstance;
    }

    private CsvToSql() {}

    /** takes a given CSV file, parses it into a list array, and inserts all records into the database
     * Code has been structured to be generic and resuable; it can handle any CSV file*/
    public boolean parseCsv(Context c, String csvFilename, String databaseName, String tableName){
        try {
            InputStream csvStream = c.getAssets().open(csvFilename);
            InputStreamReader csvStreamReader = new InputStreamReader(csvStream);
            CSVReader reader = new CSVReader(csvStreamReader);
            //potential issue: could be rewritten so that single lines from the CSV are read, and added to the DB one at a time
            //this would be significantly slower, and we would lose the safety net of SQL transactions, but it would allow the class to handle massive datasets that would
            //otherwise consume too much memory when readAll() is called. http://stackoverflow.com/questions/14988577/opencsv-not-reading-my-entire-file
            List records = reader.readAll();
            CsvDatabaseInterface database = new CsvDatabase(databaseName, tableName, c);
            return database.convertCsvToSql(records);
        }catch(FileNotFoundException e){
            return false;
        }catch(IOException e){
            return false;
        }
    }
}
