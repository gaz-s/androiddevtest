package csvtosql;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
import java.util.List;

/**
 * Created by GazAlienware on 12/8/2015.
 * Interface to the CSV conversion database, any database can be used, as long as it applies this interface
 */
public interface CsvDatabaseInterface {
    public boolean convertCsvToSql(List<String[]> records);
}
