package android.net.http;

/**
 * Created by GazAlienware on 12/9/2015.
 * This shell class. Fixes the "class file for android.net.http.AndroidHttpClient not found" error that is present in roboelectric 3.0
 * See here for more information: http://stackoverflow.com/questions/32759529/androidhttpclient-not-found-when-running-robolectric
 */
public class AndroidHttpClient {
}
