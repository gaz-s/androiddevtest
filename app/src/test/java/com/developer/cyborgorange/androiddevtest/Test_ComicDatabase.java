package com.developer.cyborgorange.androiddevtest;
//base imports for roboelectric
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;

//test method annotaions (@before, @test..)
import org.junit.*;

//test class annotations (@runwith, @config..)
import org.junit.runner.RunWith;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowToast;

import java.lang.reflect.Field;
import java.util.List;
import csvtosql.CsvToSql;

//assertThat method (there are plenty of alternative 'test' methods, google it!)
import static org.junit.Assert.*;
//is method, used inside assertThat (there are plenty of 'matching' methods, google it!)
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.greaterThan;


/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
//test class must be annotated with @runwith and @class as explained here: http://robolectric.org/getting-started/
//additionally, it seems to need the SDK flag, otherwise i get an unsupported version error
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class Test_ComicDatabase {
    //the expected number of records inside the static comic database
    private static int mRecordCount = 13459;

    @Test
    public void test_get_all_comics(){
        ComicDatabaseInterface database = ComicDatabase.getInstance(RuntimeEnvironment.application);
        ComicObject[] comics = database.getAllComics();
        assertThat("Database failed to return any comics", comics, notNullValue());
        assertThat("Database failed to return the expected number of comics", comics.length, is(mRecordCount));
    }

    @Test
    public void test_get_full_comic_record(){
        ComicDatabaseInterface database = ComicDatabase.getInstance(RuntimeEnvironment.application);
        ComicObject[] comics = database.getAllComics();
        ComicObjectExtended comic = database.getComicDetails(comics[0]);
        assertThat("Database failed to return a detailed comic object", comic, notNullValue());
        assertThat("Database returned an incomplete comic object", comic.getTitle(), notNullValue());
        assertThat("The comic object returned from the database contains the wrong record information", comic.getId(), is(comics[0].getId()));
    }

    @Test
    public void test_get_publisher_aggregate(){
        ComicDatabaseInterface database = ComicDatabase.getInstance(RuntimeEnvironment.application);
        ComicObject[] comics = database.getAllComics();
        int publisherAggregate = database.getPublisherAggregate(comics[0]);
        assertThat("Database failed to return a valid publisher aggregate value", publisherAggregate, is(greaterThan(0)));
    }

    //testing was failing due the singleton design pattern applied to the database class
    //the issue appears to be roboelectric not retaining an instance of the singleton after a unit test finishes
    //after researching, i found some code that nullifies the singleton instance after each test, which will force reconstruction of the singleton for each unit test
    @After
    public void finishComponentTesting() {
        // sInstance is the static variable name which holds the singleton instance
        resetSingleton(ComicDatabase.class, "sInstance");
    }

    private void resetSingleton(Class clazz, String fieldName) {
        Field instance;
        try {
            instance = clazz.getDeclaredField(fieldName);
            instance.setAccessible(true);
            instance.set(null, null);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
