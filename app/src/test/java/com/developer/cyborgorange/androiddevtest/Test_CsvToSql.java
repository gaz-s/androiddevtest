package com.developer.cyborgorange.androiddevtest;

//base imports for roboelectric
import org.robolectric.RobolectricGradleTestRunner;

//test method annotaions (@before, @test..)
import org.junit.*;

//test class annotations (@runwith, @config..)
import org.junit.runner.RunWith;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

//assertThat method (there are plenty of alternative 'test' methods, google it!)
import java.util.List;


import csvtosql.CsvToSql;

import static org.junit.Assert.*;
//is method, used inside assertThat (there are plenty of 'matching' methods, google it!)
import static org.hamcrest.CoreMatchers.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
//test class must be annotated with @runwith and @class as explained here: http://robolectric.org/getting-started/
//additionally, it seems to need the SDK flag, otherwise i get an unsupported version error
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class Test_CsvToSql {
    private static final String CSV_FILENAME = "titles.csv";
    private static final String TABLE_NAME = "ComicsTable";
    private static final String DATABASE_NAME = "ComicsDatabase";

    //all test methods need to be annotated with @test
    @Test
    public void test_parseCsv() throws Exception {
        Boolean result = CsvToSql.getInstance().parseCsv(RuntimeEnvironment.application, CSV_FILENAME, DATABASE_NAME, TABLE_NAME);
        assertThat("CSV parsing operation failed..", result, is(true));
        //is 'is()' method is something called a hamcrest matcher, is() simply determines that we want to test for a value match, but there are many more for all types of situations
        //see: http://hamcrest.org/JavaHamcrest/javadoc/1.3/org/hamcrest/CoreMatchers.html
    }
}