package com.developer.cyborgorange.androiddevtest;

import android.app.Dialog;
import android.app.DialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

//base imports for roboelectric
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;

//test method annotations (@before, @test..)
import org.junit.*;

//test class annotations (@runwith, @config..)
import org.junit.runner.RunWith;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.shadows.ShadowDialog;
import org.robolectric.annotation.Config;
import org.robolectric.internal.Shadow;
import org.robolectric.shadows.ShadowToast;

//assertThat method (there are plenty of alternative 'test' methods, google it!)
import java.lang.reflect.Field;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.*;
//is method, used inside assertThat (there are plenty of 'matching' methods, google it!)
import static org.hamcrest.CoreMatchers.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */

//test class must be annotated with @runwith and @class as explained here: http://robolectric.org/getting-started/
//additionally, it seems to need the SDK flag, otherwise i get an unsupported version error
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class Test_ComicActivity {

    @Test
    public void test_is_comic_listview_displayed() throws Exception{
        ComicActivity activity = Robolectric.setupActivity(ComicActivity.class);
        ListView comicList = (ListView) activity.findViewById(R.id.listview_comics);
        assertThat("The comic listview is not present", comicList, notNullValue());
    }

    @Test
    public void test_is_comic_dialog_displayed() throws Exception{
        ComicActivity activity = Robolectric.setupActivity(ComicActivity.class);
        ListView comicList = (ListView) activity.findViewById(R.id.listview_comics);
        Shadows.shadowOf(comicList).performItemClick(0);
        ComicDetailsDialog dialog = (ComicDetailsDialog) activity.getFragmentManager().findFragmentByTag(ComicDetailsDialog.class.toString());
        assertThat("Unable to find comic dialog", dialog, notNullValue());
        assertThat("Comic dialog exists, but is not visible", dialog.isAdded(), is(true));
    }

    @Test
    public void test_is_comic_added_to_favourite() throws Exception{
        ComicActivity activity = Robolectric.setupActivity(ComicActivity.class);
        ListView comicList = (ListView) activity.findViewById(R.id.listview_comics);
        activity.onFavouriteStateChanged((ComicObject) comicList.getItemAtPosition(0), 0);
        assertThat("Comics favourite state was not changed, when passed to the statechanged callback", ((ComicObject) comicList.getItemAtPosition(0)).isFavourite(), is(true));
        assertThat("Comic added to favourites, but the user was not toasted", ShadowToast.getTextOfLatestToast(), equalTo("Comic added to favourites"));
        View favouriteIcon = comicList.getChildAt(0).findViewById(R.id.listview_item_img_favourite);
        assertThat("Comic added to favourite, but the favourite icon is not visible", favouriteIcon.getVisibility(), is(View.VISIBLE));
    }

    @Test
    public void test_is_comic_removed_from_favourite() throws Exception{
        ComicActivity activity = Robolectric.setupActivity(ComicActivity.class);
        ListView comicList = (ListView) activity.findViewById(R.id.listview_comics);
        activity.onFavouriteStateChanged((ComicObject) comicList.getItemAtPosition(0), 0);
        activity.onFavouriteStateChanged((ComicObject) comicList.getItemAtPosition(0), 0);
        assertThat("Comics favourite state was not changed, when passed to the statechanged callback", ((ComicObject) comicList.getItemAtPosition(0)).isFavourite(), is(false));
        assertThat("Comic removed from favourites, but the user was not toasted", ShadowToast.getTextOfLatestToast(), equalTo("Comic removed from favourites"));
        View favouriteIcon = comicList.getChildAt(0).findViewById(R.id.listview_item_img_favourite);
        assertThat("Comic removed from favourite, but the favourite icon is still visible", favouriteIcon.getVisibility(), is(View.INVISIBLE));
    }

    @Test
    public void test_is_comic_favourite_limit_enforced() throws Exception{
        ComicActivity activity = Robolectric.setupActivity(ComicActivity.class);
        ListView comicList = (ListView) activity.findViewById(R.id.listview_comics);
        for(int i = 0; i < 11; i++) {
            activity.onFavouriteStateChanged((ComicObject) comicList.getItemAtPosition(i), i);
        }
        assertThat("User attempted to exceed the favourites limit, but was not informed via toast", ShadowToast.getTextOfLatestToast(), containsString("Unable to add to favourites, the limit of "));
    }

    @Test
    public void test_is_favourite_comic_pinned() throws Exception{
        ComicActivity activity = Robolectric.setupActivity(ComicActivity.class);
        ListView comicList = (ListView) activity.findViewById(R.id.listview_comics);
        int id = ((ComicObject) comicList.getItemAtPosition(5)).getId();
        activity.onFavouriteStateChanged((ComicObject) comicList.getItemAtPosition(5), 5);
        //if the comic was successfully pinned, its position in the underlying array should move from 5 to 0
        int topItemId = ((ComicObject) comicList.getItemAtPosition(0)).getId();
        assertThat("Comics favourite state was not changed, when passed to the statechanged callback", ((ComicObject) comicList.getItemAtPosition(0)).isFavourite(), is(true));
        View favouriteIcon = comicList.getChildAt(0).findViewById(R.id.listview_item_img_favourite);
        assertThat("Comic added to favourite, but the favourite icon is not visible", favouriteIcon.getVisibility(), is(View.VISIBLE));
        assertThat("The id of the first item in the list does not match the id of the item added to favourite", id, is(topItemId));
    }

    @Test
    public void test_comic_dialog_add_favourite() throws Exception{
        ComicActivity activity = Robolectric.setupActivity(ComicActivity.class);
        ListView comicList = (ListView) activity.findViewById(R.id.listview_comics);
        Shadows.shadowOf(comicList).performItemClick(0);
        Dialog comicDialog = ShadowDialog.getLatestDialog();
        comicDialog.show();
        Button favouriteButton = (Button) comicDialog.findViewById(R.id.dialog_comic_btn_favourite);
        assertThat("Favourite button text does not display 'add' when viewing a non-favourite comic", favouriteButton.getText().toString(), is("Add to favourites"));
    }

    @Test
    public void test_comic_dialog_remove_favourite() throws Exception{
        ComicActivity activity = Robolectric.setupActivity(ComicActivity.class);
        ListView comicList = (ListView) activity.findViewById(R.id.listview_comics);
        ((ComicObject) comicList.getItemAtPosition(0)).setFavourite(true);
        Shadows.shadowOf(comicList).performItemClick(0);
        Dialog comicDialog = ShadowDialog.getLatestDialog();
        comicDialog.show();
        Button favouriteButton = (Button) comicDialog.findViewById(R.id.dialog_comic_btn_favourite);
        assertThat("Favourite button text does not display 'remove' when viewing a favourite comic", favouriteButton.getText().toString(), is("Remove from favourites"));
    }

    @Test
    public void test_comic_dialog_display_extended_data() throws Exception{
        ComicActivity activity = Robolectric.setupActivity(ComicActivity.class);
        ListView comicList = (ListView) activity.findViewById(R.id.listview_comics);
        ((ComicObject) comicList.getItemAtPosition(0)).setFavourite(true);
        Shadows.shadowOf(comicList).performItemClick(0);
        Dialog comicDialog = ShadowDialog.getLatestDialog();
        comicDialog.show();
        TextView txtDewey = (TextView) comicDialog.findViewById(R.id.dialog_comic_txt_dewey);
        TextView txtCountry = (TextView) comicDialog.findViewById(R.id.dialog_comic_txt_country);
        assertThat("Extended comic data (dewey) was not displayed", txtDewey.getText().toString(), notNullValue());
        assertThat("Extended comic data (country) was not displayed", txtCountry.getText().toString(), notNullValue());
    }

    @Test
    public void test_comic_dialog_display_publisher_aggregate() throws Exception{
        ComicActivity activity = Robolectric.setupActivity(ComicActivity.class);
        ListView comicList = (ListView) activity.findViewById(R.id.listview_comics);
        ((ComicObject) comicList.getItemAtPosition(0)).setFavourite(true);
        Shadows.shadowOf(comicList).performItemClick(0);
        Dialog comicDialog = ShadowDialog.getLatestDialog();
        comicDialog.show();
        TextView txtAggregate = (TextView) comicDialog.findViewById(R.id.dialog_comic_txt_aggregate);
        assertThat("Publisher aggregate was not displayed", txtAggregate.getText().toString(), notNullValue());
        assertThat("Publisher aggregate was not numerical, or <= 0", Integer.parseInt(txtAggregate.getText().toString()), is(greaterThan(0)));
    }

    //testing was failing due the singleton design pattern applied to the database class
    //the issue appears to be roboelectric not retaining an instance of teh singleton after a unit test finishes
    //after researching, i found some code that nullifies the singleton instance after each test, which will force reconstruction of the singleton for each unit test
    @After
    public void finishComponentTesting() {
        // sInstance is the static variable name which holds the singleton instance
        resetSingleton(ComicDatabase.class, "sInstance");
    }

    private void resetSingleton(Class clazz, String fieldName) {
        Field instance;
        try {
            instance = clazz.getDeclaredField(fieldName);
            instance.setAccessible(true);
            instance.set(null, null);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
